package restTemplate.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import restTemplate.request.SampleRequest;

@RestController
public class InvokeeController {

	@GetMapping(path = "/invokee")
	public String get() {
		return "Getメソッド";
	}
	
	@PostMapping(path = "/invokee")
	public String post(@ModelAttribute SampleRequest req) {
		return "Postメソッド";
	}
	
	@PatchMapping(path = "/invokee")
	public String patch(@ModelAttribute SampleRequest req) {
		return "Patchメソッド";
	}
}

package restTemplate.controller;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import restTemplate.request.SampleRequest;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;


@RestController
public class InvokerController {
	private static final String URL_INVOKEE = "http://localhost:8080/invokee";
	
	@GetMapping(path = "/get")
	public ResponseEntity<String> get() {
		RestTemplate template = new RestTemplate();
		return template.getForEntity(URL_INVOKEE,String.class);
	}
	
	@GetMapping(path = "/post")
	public ResponseEntity<String> post() {
		SampleRequest request = new SampleRequest();
		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		
	    request.setId("id");
	    bodyMap.add("id", request);

	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);
	    
		RestTemplate template = new RestTemplate();
	    return template.exchange(URL_INVOKEE, HttpMethod.POST, requestEntity, String.class);
	}
	
	@GetMapping(path = "/patch")
	public  ResponseEntity<String> test005() throws IOException {
		SampleRequest request = new SampleRequest();
		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		
	    request.setId("id");
	    bodyMap.add("id", request);

	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

	    RestTemplate template = new RestTemplate();
	    
	    // patchメソッド用。RestTemplateを書き換える。
	    HttpComponentsClientHttpRequestFactory requestFactory =
	    		new HttpComponentsClientHttpRequestFactory();
	    
	    template.setRequestFactory(requestFactory);

	    return template.exchange(URL_INVOKEE, HttpMethod.PATCH, requestEntity, String.class);
	}
}

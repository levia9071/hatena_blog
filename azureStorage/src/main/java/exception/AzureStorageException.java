package exception;

public class AzureStorageException extends RuntimeException {
    public AzureStorageException(String message){
        this(message, null);
    }

    public AzureStorageException(String message, Throwable e){
        super(message,e);
    }
}

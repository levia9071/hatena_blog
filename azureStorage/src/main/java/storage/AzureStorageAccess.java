package storage;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.file.CloudFile;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.ListFileItem;
import exception.AzureStorageException;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

public class AzureStorageAccess {
    // 接続情報
    private String storageConnectionString;
    // 接続先ディレクトリ
    private CloudFileDirectory dir;

    /**
     * コンストラクタ.
     * AzureStorageに接続するアカウントとファイル共有を取得する.
     * @param shareName ファイル共有の名前
     */
    public AzureStorageAccess(String storageConnectionString,final String shareName) {
        this(storageConnectionString,shareName, null);
    }

    /**
     * コンストラクタ.
     * AzureStorageに接続するアカウントとファイル共有を取得する.
     * @param shareName ファイル共有の名前
     */
    public AzureStorageAccess(String storageConnectionString,final String shareName, final String dirName) {
        if(!isValid(storageConnectionString) || !isValid(shareName)) {
            throw  new AzureStorageException("Null");
        }

        this.storageConnectionString = storageConnectionString;

        // ストレージアカウント情報
        CloudStorageAccount storageAccount = null;
        // 接続
        try {
            storageAccount = CloudStorageAccount.parse(storageConnectionString);
        } catch (InvalidKeyException | URISyntaxException e) {
            throw new AzureStorageException("InvalidKeyException",e);
        }

        // ルートディレクトリ
        CloudFileDirectory rootDir = null;
        // ファイル共有
        try {
            CloudFileClient fileClient = storageAccount.createCloudFileClient();

            // ルートディレクトリを格納
            rootDir = fileClient.getShareReference(shareName).getRootDirectoryReference();

            // 接続先ディレクトリを格納する.
            // ディレクトリ名がnullの場合はルートディレクトリを格納する.
            if(!isValid(dirName)){
                dir = rootDir;
            } else {
                dir = rootDir.getDirectoryReference(dirName);
            }

            // sharName内のファイル一覧を取得することで、shareNameの存在をチェック
            dir.listFilesAndDirectories();
        } catch (StorageException | URISyntaxException e) {
            throw new AzureStorageException("StorageException",e);
        }
    }

    /**
     * ルートディレクトリ内のディレクトリ・ファイル名のリストを取得する.
     * @return ディレクトリ名・ファイル名一覧
     */
    public List<String> list(){
        List<String> filePathList = new ArrayList<>();
        for (ListFileItem fileItem : dir.listFilesAndDirectories()) {
            filePathList.add(fileItem.getUri().getPath());
        }
        return filePathList;
    }

    /**
     * 単一ファイルアップロード
     * @param inputFilePath インプットとなるファイルの格納場所
     * @param outputFileName アウトプットする際のファイル名
     */
    public void upload(final String inputFilePath, final String outputFileName) {
        // 入力チェック
        if(!isValid(inputFilePath) || !isValid(outputFileName)){
            throw new AzureStorageException("Check the input Exception");
        }

        // ファイル名の重複チェック
        if(searchFile(outputFileName)){
            throw new AzureStorageException("Duplication FileName");
        }

        try {
            // AAzureStorage上のファイル名を設定する.
            CloudFile cloudFile = dir.getFileReference(outputFileName);

            // アップロードを行う.
            cloudFile.uploadFromFile(inputFilePath);
            System.out.println(outputFileName + ": Upload successful!");
        } catch (StorageException | URISyntaxException | IOException e) {
            throw new AzureStorageException("UploadException", e);
        }
    }

    /**
     * 単一ファイルのアップロード.
     * @param inputStream インプットとなるファイルストリーム
     * @param outputFileName アウトプットする際のファイル名
     */
    public void upload(final FileInputStream inputStream, final String outputFileName) {
        // 入力チェック
        if(!isValid(outputFileName)){
            throw new AzureStorageException("Check the input Exception");
        }

        // ファイル名の重複チェック
        if(searchFile(outputFileName)){
            throw new AzureStorageException("Duplication FileName");
        }

        try {
            // AAzureStorage上のファイル名を設定する.
            CloudFile cloudFile = dir.getFileReference(outputFileName);

            // アップロードを行う.
            cloudFile.upload(inputStream,inputStream.available());
            System.out.println(outputFileName + ": Upload successful!");
        } catch (StorageException | URISyntaxException | IOException e) {
            throw new AzureStorageException("UploadException", e);
        }
    }

    /**
     * 単一ファイルのダウンロード
     * @param fileName ダウンロードするファイル名
     */
    public void download(final String fileName) {
        // 入力チェック
        if(!isValid(fileName)){
            throw new AzureStorageException("Check the input Exception");
        }

        // ファイルの存在チェック
        if(!searchFile(fileName)){
            throw new AzureStorageException("no Search File");
        }

        try {
            CloudFile file = dir.getFileReference(fileName);

            System.out.println("[DownloadFile:FileName]"+file.getName());
            System.out.println("[DownloadFile:TextData]"+file.downloadText());
        } catch (StorageException | URISyntaxException | IOException e) {
            throw new AzureStorageException("DownloadException", e);
        }
    }

    /**
     * 単一ファイルの削除
     * @param fileName 削除するファイル名
     */
    public void delete(final String fileName) {
        // 入力チェック
        if(!isValid(fileName)){
            throw new AzureStorageException("Check the input Exception");
        }

        // ファイルの存在チェック
        if(!searchFile(fileName)){
            throw new AzureStorageException("no Search File");
        }

        CloudFile file;

        try {
            file = dir.getFileReference(fileName);
            if ( file.deleteIfExists() ) {
                System.out.println(fileName + " was deleted");
            }
        } catch (URISyntaxException | StorageException e) {
            throw new AzureStorageException("DeleteException", e);
        }
    }

    /**
     * ファイル共有内にファイル名が存在するか検索を行なう.
     * @param fileName 検索するファイル名
     * @return 存在する場合、trueを返却する.
     */
    private boolean searchFile(String fileName){
        List<String> list = list();

        for(String filePath: list){
            if(filePath.contains(fileName)){
                return true;
            }
        }
        return false;
    }

    /**
     * 入力チェック
     * @param str チェックする文字列
     * @return 条件を満たす場合はtrue、それ以外の場合はfalse.
     */
    private boolean isValid(String str){
        if(str == null || "".equals(str.trim())){
            return false;
        }
        return true;
    }
}

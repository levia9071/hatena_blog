# はじめに
LEViAが管理する[はてなブログ](https://levia9071.hatenablog.com/)用のリポジトリ。

# フォルダ説明
| フォルダ名 | 説明 |
| :-- | :-- |
| restTemplate | [RestTemplateでつまったことが多いからまとめてみた](https://levia9071.hatenablog.com/entry/2019/11/23/141603)のサンプルソース |
| azureStorage | Azure初心者が無料ではじめてみた。～AzureStorage編～のサンプル |
| kotlin/webview | [リクエストヘッダーをカスタムして、Webviewで投げつけたい](https://levia9071.hatenablog.com/entry/2020/07/19/214626) |
|  |  |
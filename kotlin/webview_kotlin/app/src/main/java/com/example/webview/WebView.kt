package com.example.webview

import android.app.Activity
import android.os.Bundle
import android.webkit.WebView

class WebView : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        WebView.setWebContentsDebuggingEnabled(true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        val myWebView = findViewById<WebView>(R.id.webview)

        val extraHeaders: MutableMap<String, String> = HashMap()
        extraHeaders["ORIGINAL_HEADER"] = "original data"

//        val url = "https://uchy.me/tools/request_headers.html"
        val url = "https://www.yahoo.co.jp"
        myWebView.loadUrl(url, extraHeaders)
    }
}
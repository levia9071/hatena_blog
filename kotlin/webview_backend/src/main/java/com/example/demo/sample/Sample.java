package com.example.demo.sample;

import org.springframework.web.bind.annotation.*;

@RestController
public class Sample {
    @GetMapping("/test")
    public String test(@RequestHeader("ORIGINAL_HEADER") String userAgent){
        return userAgent;
    }

    @GetMapping("/test1")
    public String test(){
        return "test";
    }
}